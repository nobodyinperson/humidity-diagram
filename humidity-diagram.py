#!/usr/bin/env python3
# core modules
import textwrap
import argparse
import logging

# external modules
import numpy as np
from scipy.interpolate import interp1d

logging.basicConfig(level=logging.DEBUG)


# Options
parser = argparse.ArgumentParser(
    description=textwrap.dedent(
        """
        Plot a humidity diagram
        """
    )
)

parser.add_argument(
    "-T",
    "--temp_range",
    help="temperature range [°C]",
    nargs=2,
    default=[-5, 35],
    type=float,
)
parser.add_argument(
    "-e",
    "--vapour_range",
    help="water vapour pressure range [hPa]",
    nargs=2,
    type=float,
    default=[0, 40],
)
parser.add_argument("-r", "--res", help="resolution", type=int, default=1000)
parser.add_argument("-l", "--log", help="logarithmic plot", action="store_true")
parser.add_argument(
    "--separate", help="separate dew point and absolute humidity", action="store_true"
)
parser.add_argument("--labelsize", help="label font size", type=float, default=7)
parser.add_argument(
    "--dewpoints",
    help="dewpoint levels",
    type=float,
    nargs="+",
    default=[-22, -12, -5, 0, 5, 10, 12, 15, 18, 20, 22, 23, 25, 27, 28, 29, 30],
)
parser.add_argument("--negative", help="negative line style", default="solid")
parser.add_argument(
    "-o", "--output", default="humidity-diagram.pdf", help="output file"
)
parser.add_argument("--show", help="show the plot?", action="store_true")

# parse the arguments
args = parser.parse_args()

import matplotlib

if not args.show:  # use pdf backend if we don't show the plot
    matplotlib.use("pdf")
import matplotlib.pyplot as plt
from matplotlib import rc

rc("font", **{"family": ["Helvetica"], "sans-serif": ["Helvetica"]})
rc("text", usetex=True)
plt.rcParams["pgf.preamble"] = r"\usepackage{siunitx}"
plt.rcParams["text.latex.preamble"] = r"\usepackage{siunitx}"

# generate data
# constants
kelvin_offset = 273.15  # Kelvin
R_w = 461.4  # J/(kg*K)

cel2kel = lambda T: T + kelvin_offset
kel2cel = lambda T: T - kelvin_offset
pa2hpa = lambda p: p * 1e-2
hpa2pa = lambda p: p * 1e2


def e_magnus(T, over="water"):
    A = 6.112e2  # Pa
    B = 17.62 if over == "water" else 22.46  # Einheitenlos
    C = 243.12 if over == "water" else 272.62  # °C
    T = kel2cel(T)  # convert to Celsius
    return A * np.exp((B * T) / (C + T))


def rel_humidity(T, e):
    return e / e_magnus(T)


def abs_humidity(T, e):
    return e / (R_w * T)


T_seq = cel2kel(np.linspace(*args.temp_range, args.res))
e_seq = hpa2pa(np.linspace(*args.vapour_range, args.res))

# magnus inversion
dewpoint_T_seq = cel2kel(
    np.linspace(args.temp_range[0] - 20, args.temp_range[1] + 20, args.res * 2)
)
dewpointquotient_inversion = interp1d(
    x=e_magnus(dewpoint_T_seq) / dewpoint_T_seq,
    y=dewpoint_T_seq,
    kind="linear",
    bounds_error=False,
)


def dewpoint(T, e):
    return dewpointquotient_inversion(e / T)


T, e = np.meshgrid(T_seq, e_seq)

T_d = dewpoint(T, e)

rho_w = abs_humidity(T, e)

RH = rel_humidity(T, e)
RH = np.where(RH > 1, 1 + 1e-3, RH)

# Plot
matplotlib.rcParams["contour.negative_linestyle"] = args.negative


def fakeplot(line, label, color):
    return ax.plot(
        (-1e9, -1e9),
        (-1e-9, -1e-9),
        c=color,
        lw=line.get_linewidth()[0],
        label=label,
    )


plt.close("all")
fig, ax = plt.subplots()
ylim = args.vapour_range
if args.log:
    ax.set_yscale("log")
    ylim[0] = max(10, ylim[0])
ax.set_ylim(ylim)
ax.set_xlim(args.temp_range)
ax.grid(color="darkgray", linestyle=":", linewidth=1)
ax.set_title(r"Humidity")
ax.set_xlabel(r"Temperature [$\si{\celsius}$]")
ax.set_ylabel(r"water vapour pressure [$\si{\hecto\pascal}$]")


def dewpoint_formatter(dewpoint):
    fmt = r"$T_d=\SI{%.0f}{\celsius}$" % dewpoint
    if not args.separate:
        rho_w_here = e_magnus(cel2kel(dewpoint)) / cel2kel(dewpoint) / R_w * 1e3
        fmt += (
            r" / $\rho_w=\SI[per-mode=symbol]" "{%.0f}{\gram\per\cubic\metre}$"
        ) % rho_w_here
    return fmt


dewpoint_contour_kwargs = {"levels": args.dewpoints} if args.dewpoints else {}
dewpoint_contour = ax.contour(
    kel2cel(T),  # x
    pa2hpa(e),  # y
    kel2cel(T_d),  # z
    colors=(color := "darkblue"),
    **dewpoint_contour_kwargs,
)
dewpointlabel = "dewpoint"
if not args.separate:
    dewpointlabel += " / absolute humidity"
fakeplot(dewpoint_contour.collections[0], label=dewpointlabel, color=color)
ax.clabel(dewpoint_contour, fontsize=args.labelsize, inline=1, fmt=dewpoint_formatter)

if args.separate:
    rho_w_contour = ax.contour(
        kel2cel(T),  # x
        pa2hpa(e),  # y
        rho_w * 1e3,  # z
        colors=(color := "darkgreen"),
    )
    fakeplot(rho_w_contour.collections[0], label="absolute humidity", color=color)
    ax.clabel(
        rho_w_contour,
        fontsize=args.labelsize,
        inline=1,
        fmt=r"$\rho_w=\SI[per-mode=symbol]{%.0f}{\gram\per\cubic\metre}$",
    )


ax.legend(
    loc="upper left",
    framealpha=1,
)

rh_contour = ax.contour(
    kel2cel(T),  # x
    pa2hpa(e),  # y
    RH * 100,  # z
    colors=(color := "darkorange"),
    levels=np.linspace(10, 100, 10),
)
fakeplot(rh_contour.collections[0], label="relative humidity", color=color)
ax.clabel(rh_contour, fontsize=args.labelsize, inline=1, fmt=r"$%.0f\%%$")

ax.legend(
    loc="upper left",
    framealpha=1,
)

logging.info("Saving plot to '{}'".format(args.output))
fig.savefig(args.output)
logging.info("Plot to '{}'".format(args.output))
plt.close("all")
