#!/usr/bin/make -f

PLOTTER = humidity-diagram.py

PLOT_BOILING = humidity-diagram-boiling.pdf
PLOT_NORMAL = humidity-diagram-normal.pdf

.PHONY: all
all: $(PLOT_NORMAL) $(PLOT_BOILING)

$(PLOT_NORMAL): $(PLOTTER)
	./$(PLOTTER) \
		--dewpoints `seq -25 10 -5` `seq 0 3 42` \
		--labelsize 7 \
		--negative dashed \
		-T -5 40 \
		-e 0 70 \
		-o $@

$(PLOT_BOILING): $(PLOTTER)
	./$(PLOTTER) \
		-e 500 1200 \
		-T 70 120 \
		-o $@


.PHONY: clean
clean:
	rm -f *.pdf
